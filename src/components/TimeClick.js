import { Component } from "react";

class TimeClick extends Component {
    constructor(props){
        super(props);
        this.state ={
            date: []
        }
    }
    onBtnDateAdd = () => {
        const dateAdd = new Date().toLocaleTimeString();
        this.setState ({
            date: [...this.state.date, dateAdd]
        })
    }
    render(){
        const dataUpdate =  this.state.date;
        return(
            <div style={{display:"flex", flexDirection: "column", alignItems:"center",margin:"auto"}}>
                <div>
                    <ul style={{padding:"0"}}>List:
                        {dataUpdate.map((data, index) =><li style={{listStyle:"none"}} key={index}> {index +1}  {data} </li>)}
                    </ul>   
                </div>
                <div>
                    <button onClick={this.onBtnDateAdd} style={{marginLeft:"70px"}}>Add To List</button>
                </div>
                
            </div>
        )
    }
}
export default TimeClick;